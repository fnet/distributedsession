package com.darkidiot.session.conf;

/**
 * 常量类
 * Copyright (c) for darkidiot
 * Date:2017/4/17
 * Author: <a href="darkidiot@icloud.com">darkidiot</a>
 * School: CUIT
 * Desc:
 */
public interface Constant {
    String configurationFileName = "session.properties";
}
