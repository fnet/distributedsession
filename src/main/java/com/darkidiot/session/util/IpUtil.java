package com.darkidiot.session.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ip工具类
 * Copyright (c) for darkidiot
 * Date:2017/4/19
 * Author: <a href="darkidiot@icloud.com">darkidiot</a>
 * School: CUIT
 * Desc:
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class IpUtil {

    public static Boolean isIp(String addr) {

        if (addr.length() < 7 || addr.length() > 15) {
            return false;
        }

        String rexp = "([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}";

        Pattern pat = Pattern.compile(rexp);

        Matcher mat = pat.matcher(addr);

        return mat.find();
    }

    /**
     * @param args
     * @Title : main
     * @Type : IpAddress
     * @date : 2014年3月4日 下午10:55:06
     * @Description : IP可能的范围是0-255.0-255.0-255.0-255
     */
    public static void main(String[] args) {
        /**
         * 符合IP地址的范围
         */
        String oneAddress = "10.127.30.45";
        /**
         * 符合IP地址的长度范围但是不符合格式
         */
        String twoAddress = "127.30.45";
        /**
         * 不符合IP地址的长度范围
         */
        String threeAddress = "7.0.4";
        /**
         * 不符合IP地址的长度范围但是不符合IP取值范围
         */
        String fourAddress = "255.255.255.2567";

        //判断oneAddress是否是IP
        log.info(isIp(oneAddress)+"");

        //判断twoAddress是否是IP
        log.info(isIp(twoAddress)+"");

        //判断threeAddress是否是IP
        log.info(isIp(threeAddress)+"");

        //判断fourAddress是否是IP
        log.info(isIp(fourAddress)+"");
    }

}
